package nl.utwente.di.bookQuote;

import jakarta.servlet.http.HttpServlet;

import java.util.HashMap;

public class Calculator  {

    public double transformation(String degree) {
        return Double.parseDouble(degree) * 9/5 + 32;
    }
}
